import React, { useState } from 'react';
import {
  Box,
  Grid,
  Typography,
  Select,
  MenuItem,
  Button,
  Link,
  Paper
} from '@mui/material';

import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { TimePicker } from '@mui/x-date-pickers/TimePicker';

import MeetingSchedulerModal from './MeetingSchedulerModal';
import InterviewerTable from './interviewerTable';

const FormRepeater: React.FC = () => {
  const [rows, setRows] = useState([{ id: 1 }]);

  const handleAddRow = () => {
    setRows([...rows, { id: rows.length + 1 }]);
  };

  const handleDeleteRow = (id: number) => {
    let newRows = [...rows];
    newRows = newRows.filter((row) => row.id !== id);
    setRows(newRows);
  };

  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <>
      <Box sx={{ mt: 3 }}>
        {rows.map((row) => (
          <Paper key={row.id} elevation={3} sx={{ p: 2, mb: 2 }}>
            <Grid container spacing={2} alignItems="center">
              <Grid item xs={3}>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DemoContainer components={['TimePicker']}>
                    <TimePicker
                      slotProps={{ textField: { size: 'small' } }}
                      label="Start Time"
                      fullWidth
                    />
                  </DemoContainer>
                </LocalizationProvider>
              </Grid>
              <Grid item xs={3}>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DemoContainer components={['TimePicker']}>
                    <TimePicker
                      slotProps={{ textField: { size: 'small' } }}
                      label="End Time"
                      fullWidth
                    />
                  </DemoContainer>
                </LocalizationProvider>
              </Grid>
              <Grid item xs={3} align="center">
                <Link onClick={handleOpen} underline="hover">
                  Scheduler Management
                </Link>
              </Grid>
              <Grid item xs={3} align="center">
                {row.id != 1 && (
                  <Link
                    onClick={() => handleDeleteRow(row.id)}
                    underline="hover"
                  >
                    Delete
                  </Link>
                )}
              </Grid>
            </Grid>
          </Paper>
        ))}
        <Button onClick={handleAddRow} variant="contained" sx={{ mt: 2 }}>
          Add More
        </Button>
      </Box>
      <MeetingSchedulerModal open={open} handleClose={handleClose} />
    </>
  );
};

const DynamicForm: React.FC = () => {
  const [showFormRepeater, setShowFormRepeater] = useState(false);
  const [selectedOption, setSelectedOption] = useState('');

  const handleSelectChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setSelectedOption(event.target.value as string);
    setShowFormRepeater(true);
  };

  return (
    <>
      <Box
        sx={{
          p: 4,
          maxWidth: '100%'
        }}
      >
        <Grid container spacing={2} alignItems="center">
          <Grid item>
            <Typography variant="h6">Select Interview Type:</Typography>
          </Grid>
          <Grid item>
            <Select
              size="small"
              value={selectedOption}
              onChange={handleSelectChange}
              displayEmpty
              sx={{ minWidth: 200 }}
            >
              <MenuItem value="option1">Mobile - 15 Mins</MenuItem>
              <MenuItem value="option2">Technical 1 - 30 Mins</MenuItem>
              <MenuItem value="option3">Technical 2 - 60 Mins</MenuItem>
              <MenuItem value="option4">HR/Managerial - 30 Mins</MenuItem>
            </Select>
          </Grid>
        </Grid>
        {showFormRepeater && <FormRepeater />}
        {showFormRepeater && <InterviewerTable />}
      </Box>
    </>
  );
};

export default DynamicForm;
