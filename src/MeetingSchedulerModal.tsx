import React, { useState } from 'react';
import {
  Box,
  Button,
  Checkbox,
  FormControlLabel,
  Grid,
  MenuItem,
  Modal,
  Select,
  TextField,
  Typography,
  InputLabel,
  FormControl
} from '@mui/material';
import { TimePicker } from '@mui/x-date-pickers/TimePicker';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';

interface MeetingSchedulerModalProps {
  open: boolean;
  handleClose: () => void;
}

const MeetingSchedulerModal: React.FC<MeetingSchedulerModalProps> = ({
  open,
  handleClose
}) => {
  const [isRecurring, setIsRecurring] = useState(false);
  const [recurrenceType, setRecurrenceType] = useState('daily');
  const [recurrenceDetails, setRecurrenceDetails] = useState({
    daily: { interval: 1 },
    weekly: { interval: 1, days: [] as string[] },
    monthly: {
      interval: 1,
      option: 'day',
      day: 1,
      week: 'First',
      weekDay: 'Sunday'
    }
  });

  const handleRecurrenceChange = (
    event: React.ChangeEvent<{ value: unknown }>
  ) => {
    setRecurrenceType(event.target.value as string);
  };

  const handleRecurrenceDetailsChange = (field: string, value: any) => {
    setRecurrenceDetails((prevDetails) => ({
      ...prevDetails,
      [recurrenceType]: {
        ...prevDetails[recurrenceType],
        [field]: value
      }
    }));
  };

  return (
    <Modal open={open} onClose={handleClose}>
      <Box
        sx={{
          p: 4,
          backgroundColor: 'white',
          borderRadius: 2,
          maxWidth: 600,
          margin: 'auto',
          mt: 5
        }}
      >
        <Typography variant="h6" gutterBottom>
          Schedule a Meeting
        </Typography>
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={['DatePicker']}>
                <DatePicker
                  slotProps={{ textField: { size: 'small' } }}
                  label="Start Date"
                  size="small"
                  renderInput={(params) => <TextField {...params} fullWidth />}
                />
              </DemoContainer>
            </LocalizationProvider>
          </Grid>
          <Grid item xs={6}>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={['TimePicker']}>
                <TimePicker
                  slotProps={{ textField: { size: 'small' } }}
                  label="Start Time"
                  size="small"
                  renderInput={(params) => <TextField {...params} fullWidth />}
                />
              </DemoContainer>
            </LocalizationProvider>
          </Grid>
          <Grid item xs={6}>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={['DatePicker']}>
                <DatePicker
                  slotProps={{ textField: { size: 'small' } }}
                  label="End Date"
                  size="small"
                  renderInput={(params) => <TextField {...params} fullWidth />}
                />
              </DemoContainer>
            </LocalizationProvider>
          </Grid>
          <Grid item xs={6}>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={['TimePicker']}>
                <TimePicker
                  slotProps={{ textField: { size: 'small' } }}
                  label="End Time"
                  size="small"
                  renderInput={(params) => <TextField {...params} fullWidth />}
                />
              </DemoContainer>
            </LocalizationProvider>
          </Grid>
          <Grid item xs={12}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={isRecurring}
                  onChange={() => setIsRecurring(!isRecurring)}
                />
              }
              label="Recurring"
            />
          </Grid>
          {isRecurring && (
            <>
              <Grid item xs={12}>
                <Select
                  value={recurrenceType}
                  onChange={handleRecurrenceChange}
                  displayEmpty
                  fullWidth
                  size="small"
                >
                  <MenuItem value="daily">Daily</MenuItem>
                  <MenuItem value="weekly">Weekly</MenuItem>
                  <MenuItem value="monthly">Monthly</MenuItem>
                </Select>
              </Grid>
              {recurrenceType === 'daily' && (
                <Grid item xs={12}>
                  <FormControl fullWidth>
                    <InputLabel id="recurrenceDetails-daily-interval-label">
                      Repeat every
                    </InputLabel>
                    <Select
                      size="small"
                      labelId="recurrenceDetails-daily-interval-label"
                      id="recurrenceDetails-daily-interval"
                      label="Repeat every"
                      value={recurrenceDetails.daily.interval}
                      onChange={(e) =>
                        handleRecurrenceDetailsChange(
                          'interval',
                          e.target.value
                        )
                      }
                      fullWidth
                    >
                      {Array.from({ length: 15 }, (_, i) => i + 1).map(
                        (day) => (
                          <MenuItem key={day} value={day}>
                            {day} day{day > 1 ? 's' : ''}
                          </MenuItem>
                        )
                      )}
                    </Select>
                  </FormControl>
                </Grid>
              )}
              {recurrenceType === 'weekly' && (
                <>
                  <Grid item xs={12}>
                    <FormControl fullWidth>
                      <InputLabel id="recurrenceDetails-weekly-interval-label">
                        Repeat every
                      </InputLabel>
                      <Select
                        size="small"
                        labelId="recurrenceDetails-weekly-interval-label"
                        id="recurrenceDetails-weekly-interval"
                        label="Repeat every"
                        value={recurrenceDetails.weekly.interval}
                        onChange={(e) =>
                          handleRecurrenceDetailsChange(
                            'interval',
                            e.target.value
                          )
                        }
                        fullWidth
                      >
                        {Array.from({ length: 12 }, (_, i) => i + 1).map(
                          (week) => (
                            <MenuItem key={week} value={week}>
                              {week} week{week > 1 ? 's' : ''}
                            </MenuItem>
                          )
                        )}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography>Days of the Week</Typography>
                    <Box sx={{ display: 'flex', flexWrap: 'wrap' }}>
                      {[
                        'Sunday',
                        'Monday',
                        'Tuesday',
                        'Wednesday',
                        'Thursday',
                        'Friday',
                        'Saturday'
                      ].map((day) => (
                        <FormControlLabel
                          key={day}
                          control={
                            <Checkbox
                              checked={recurrenceDetails.weekly.days.includes(
                                day
                              )}
                              onChange={() => {
                                const newDays =
                                  recurrenceDetails.weekly.days.includes(day)
                                    ? recurrenceDetails.weekly.days.filter(
                                        (d) => d !== day
                                      )
                                    : [...recurrenceDetails.weekly.days, day];
                                handleRecurrenceDetailsChange('days', newDays);
                              }}
                            />
                          }
                          label={day}
                        />
                      ))}
                    </Box>
                  </Grid>
                </>
              )}
              {recurrenceType === 'monthly' && (
                <>
                  <Grid item xs={12}>
                    <FormControl fullWidth>
                      <InputLabel id="recurrenceDetails-monthly-interval-label">
                        Repeat every
                      </InputLabel>
                      <Select
                        size="small"
                        labelId="recurrenceDetails-monthly-interval-label"
                        id="recurrenceDetails-monthly-interval"
                        label="Repeat every"
                        value={recurrenceDetails.monthly.interval}
                        onChange={(e) =>
                          handleRecurrenceDetailsChange(
                            'interval',
                            e.target.value
                          )
                        }
                        fullWidth
                      >
                        {Array.from({ length: 4 }, (_, i) => i + 1).map(
                          (month) => (
                            <MenuItem key={month} value={month}>
                              {month} month{month > 1 ? 's' : ''}
                            </MenuItem>
                          )
                        )}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={recurrenceDetails.monthly.option === 'day'}
                          onChange={() =>
                            handleRecurrenceDetailsChange('option', 'day')
                          }
                        />
                      }
                      label="Day of the Month"
                    />
                    {recurrenceDetails.monthly.option === 'day' && (
                      <Select
                        size="small"
                        value={recurrenceDetails.monthly.day}
                        onChange={(e) =>
                          handleRecurrenceDetailsChange('day', e.target.value)
                        }
                        fullWidth
                      >
                        {Array.from({ length: 31 }, (_, i) => i + 1).map(
                          (day) => (
                            <MenuItem key={day} value={day}>
                              {day}
                            </MenuItem>
                          )
                        )}
                      </Select>
                    )}
                  </Grid>
                  <Grid item xs={12}>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={recurrenceDetails.monthly.option === 'week'}
                          onChange={() =>
                            handleRecurrenceDetailsChange('option', 'week')
                          }
                        />
                      }
                      label="Week of the Month"
                    />
                    {recurrenceDetails.monthly.option === 'week' && (
                      <Grid container spacing={2}>
                        <Grid item xs={6}>
                          <Select
                            size="small"
                            value={recurrenceDetails.monthly.week}
                            onChange={(e) =>
                              handleRecurrenceDetailsChange(
                                'week',
                                e.target.value
                              )
                            }
                            fullWidth
                          >
                            {['First', 'Second', 'Third', 'Last'].map(
                              (week) => (
                                <MenuItem key={week} value={week}>
                                  {week}
                                </MenuItem>
                              )
                            )}
                          </Select>
                        </Grid>
                        <Grid item xs={6}>
                          <Select
                            size="small"
                            value={recurrenceDetails.monthly.weekDay}
                            onChange={(e) =>
                              handleRecurrenceDetailsChange(
                                'weekDay',
                                e.target.value
                              )
                            }
                            fullWidth
                          >
                            {[
                              'Sunday',
                              'Monday',
                              'Tuesday',
                              'Wednesday',
                              'Thursday',
                              'Friday',
                              'Saturday'
                            ].map((day) => (
                              <MenuItem key={day} value={day}>
                                {day}
                              </MenuItem>
                            ))}
                          </Select>
                        </Grid>
                      </Grid>
                    )}
                  </Grid>
                </>
              )}
              <Grid item xs={12}>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DemoContainer components={['DatePicker']}>
                    <DatePicker
                      slotProps={{ textField: { size: 'small' } }}
                      label="Recurrence End Date"
                      renderInput={(params) => (
                        <TextField {...params} fullWidth />
                      )}
                    />
                  </DemoContainer>
                </LocalizationProvider>
              </Grid>
            </>
          )}
        </Grid>
        <Box sx={{ mt: 3, textAlign: 'right' }}>
          <Button onClick={handleClose} sx={{ mr: 1 }}>
            Cancel
          </Button>
          <Button
            variant="contained"
            onClick={() => {
              console.log('Save meeting details');
              handleClose();
            }}
          >
            Save
          </Button>
        </Box>
      </Box>
    </Modal>
  );
};

export default MeetingSchedulerModal;
