import { makeStyles } from '@mui/styles';
import { Divider, Grid, Paper } from '@mui/material';
import Chat from './chat.tsx';
import Profile from './profile';
import DynamicForm from './dynamicForm';
import SuggestTimeSlots from './suggestTimeSlots';
const useStyles = makeStyles({
  table: {
    minWidth: 650
  },
  chatSection: {
    width: '100%'
  },
  headBG: {
    backgroundColor: '#e0e0e0'
  },
  borderRight500: {
    borderRight: '1px solid #e0e0e0'
  },
  messageArea: {
    height: '70vh',
    overflowY: 'auto'
  }
});

function App() {
  const classes: any = useStyles();

  return (
    <>
      <Profile
        name="Tanvi Jain"
        email="tanvi1.jain@deshaw.com"
        imageUrl="https://via.placeholder.com/75"
      />
      <Divider />
      <Grid container component={Paper} className={classes.chatSection}>
        <Grid item xs={3} className={classes.borderRight500}>
          <Chat />
        </Grid>
        <Grid item xs={6} className={classes.borderRight500}>
          <DynamicForm />
        </Grid>
        <Grid item xs={3}>
          <SuggestTimeSlots />
        </Grid>
      </Grid>
    </>
  );
}

export default App;
