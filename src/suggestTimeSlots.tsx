import React from 'react';
import { Box, Card, CardContent, Grid, Typography } from '@mui/material';
import { DatePicker, TimePicker } from '@mui/lab';

interface TimeSlot {
  startDate: string;
  startTime: string;
  endDate: string;
  endTime: string;
  interviewer: string;
}

const timeSlots: TimeSlot[] = [
  {
    startDate: '2024-06-01',
    startTime: '10:00 AM',
    endDate: '2024-06-01',
    endTime: '11:00 AM',
    interviewer: 'John Doe'
  },
  {
    startDate: '2024-06-02',
    startTime: '02:00 PM',
    endDate: '2024-06-02',
    endTime: '03:00 PM',
    interviewer: 'Jane Smith'
  }
  // Add more time slots as needed
];

const SuggestTimeSlots: React.FC = () => {
  return (
    <Box sx={{ mt: 1, p: 2 }}>
      <Typography variant="h6" gutterBottom>
        Suggested Time Slots
      </Typography>
      {timeSlots.map((slot, index) => (
        <Card elevation={2} key={index} sx={{ mb: 2 }}>
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Typography variant="subtitle1" gutterBottom>
                  Start Date
                </Typography>
                <Typography variant="body2">{slot.startDate}</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="subtitle1" gutterBottom>
                  Start Time
                </Typography>
                <Typography variant="body2">{slot.startTime}</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="subtitle1" gutterBottom>
                  End Date
                </Typography>
                <Typography variant="body2">{slot.endDate}</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="subtitle1" gutterBottom>
                  End Time
                </Typography>
                <Typography variant="body2">{slot.endTime}</Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography variant="subtitle1" gutterBottom>
                  Interviewer
                </Typography>
                <Typography variant="body2">{slot.interviewer}</Typography>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      ))}
    </Box>
  );
};

export default SuggestTimeSlots;
