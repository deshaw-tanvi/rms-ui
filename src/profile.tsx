import React from 'react';
import { Grid, Typography, Box } from '@mui/material';

interface ProfileProps {
  name: string;
  email: string;
  imageUrl: string;
}

const Profile: React.FC<ProfileProps> = ({ name, email, imageUrl }) => {
  return (
    <Box sx={{ display: 'flex' }}>
      <Grid container spacing={2} alignItems="center">
        <Grid item xs={1}>
          <Box
            component="img"
            src={imageUrl}
            alt={name}
            sx={{
              borderRadius: '50%'
            }}
          />
        </Grid>
        <Grid item xs={3}>
          <Typography variant="h6" component="div" sx={{ fontWeight: 'bold' }}>
            {name}
          </Typography>
          <Typography variant="body2" color="textSecondary">
            {email}
          </Typography>
        </Grid>
        <Grid item xs={4}>
          <Typography variant="body2" color="textSecondary">
            detail 1: abc
          </Typography>
          <Typography variant="body2" color="textSecondary">
            detail 2: abc
          </Typography>
        </Grid>
        <Grid item xs={4}>
          <Typography variant="body2" color="textSecondary">
            detail 3: abc
          </Typography>
          <Typography variant="body2" color="textSecondary">
            detail 4: abc
          </Typography>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Profile;
